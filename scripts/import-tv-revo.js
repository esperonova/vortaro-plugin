const fsP = require("fs/promises");
const https = require("https");
const path = require("path");

Promise.all(["be", "cs", "de", "en", "es", "fr", "hu", "nl", "pl", "pt", "ru", "sk"].map(lang => {
	return new Promise((resolve, reject) => {
		https.get("https://www.tujavortaro.net/revo/revo-" + lang + ".js", resolve).on("error", reject);
	})
		.then(res => {
			if(res.statusCode !== 200) throw new Error("Failed to download");

			return new Promise((resolve, reject) => {
				const chunks = [];
				res.on("error", reject);
				res.on("data", chunk => {
					chunks.push(chunk);
				});
				res.on("end", () => {
					resolve(Buffer.concat(chunks));
				});
			});
		})
		.then(content => {
			content = content.toString();
			const start = content.indexOf("[");
			const end = content.indexOf(",\n]");
			const src = content.substring(start, end) + "]";
			const j = JSON.parse(src);

			const output = j.map(([key, ...values]) => {
				return key + " : " + values.join(", ");
			}).join("\n");

			return fsP.writeFile(path.resolve(__dirname, "../src/main/resources/revo-" + lang + ".txt"), output);
		});
}));
