package net.esperonova.plugins.vortaro;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LingvoCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length != 1) return false;

        if(!(sender instanceof Player)) {
            sender.sendMessage("§cNur ludantoj povas uzi ĉi tiun komandon");
            return true;
        }

        for (String language : VortaroPlugin.LANGUAGES) {
            if(language.equals(args[0])) {
                var section = VortaroPlugin.userConfig.getConfigurationSection(((Player) sender).getUniqueId().toString());
                if(section == null) {
                    section = VortaroPlugin.userConfig.createSection(((Player) sender).getUniqueId().toString());
                }
                section.set("language", language);
                VortaroPlugin.saveUserConfig();

                sender.sendMessage("§aLa vortara lingvo estis ŝanĝita al §6§l" + language + "§r§a.");

                return true;
            }
        }

        sender.sendMessage("§cNesubtenata lingvo. Subtenataj lingvoj estas: §6" + String.join("§c, §6", VortaroPlugin.LANGUAGES));
        return true;
    }
}
