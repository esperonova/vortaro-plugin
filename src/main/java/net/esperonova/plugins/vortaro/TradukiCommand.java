package net.esperonova.plugins.vortaro;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.HashMap;

import javax.annotation.Nullable;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TradukiCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length != 1) return false;

        String language = "en";
        if(sender instanceof Player) {
            var section = VortaroPlugin.userConfig.getConfigurationSection(((Player) sender).getUniqueId().toString());
            if(section != null) {
                var value = section.getString("language");
                if(value != null) language = value;
            }
        }

        var dictionary = VortaroPlugin.dictionaries.get(language);

        var definition = findDefinition(dictionary, args[0]);

        if(definition == null) {
            sender.sendMessage("§cNenio troviĝis.");
        }
        else {
            sender.sendMessage("§a" + definition);
        }

        return true;
    }

	private String handleX(String src) {
		if(src.length() > 1) {
			StringBuilder builder = new StringBuilder(src.length());
			char prev = 0;
			var iter = new StringCharacterIterator(src);
			prev = iter.first();

			char current = iter.next();
			while(current != CharacterIterator.DONE) {
				if(current == 'x' || current == 'X') {
					char result = 0;
					if(prev == 'c') result = 'ĉ';
					else if(prev == 'C') result = 'Ĉ';
					else if(prev == 'g') result = 'ĝ';
					else if(prev == 'G') result = 'Ĝ';
					else if(prev == 'h') result = 'ĥ';
					else if(prev == 'H') result = 'Ĥ';
					else if(prev == 'j') result = 'ĵ';
					else if(prev == 'J') result = 'Ĵ';
					else if(prev == 's') result = 'ŝ';
					else if(prev == 'S') result = 'Ŝ';
					else if(prev == 'u') result = 'ŭ';
					else if(prev == 'U') result = 'Ŭ';

					if(result == 0) {
						builder.append(prev);
						prev = current;
					}
					else {
						builder.append(result);
						prev = iter.next();
					}
				}
				else {
					builder.append(prev);
					prev = current;
				}
				current = iter.next();
			}
			if(prev != CharacterIterator.DONE) builder.append(prev);

			return builder.toString();
		}
		else {
			return src;
		}
	}

	@Nullable
	private String findDefinition(HashMap<String, String> dictionary, String search) {
		var definition = findDefinitionInner(dictionary, search);
		if(definition == null) {
			var withoutX = handleX(search);
			definition = findDefinitionInner(dictionary, withoutX);
			if(definition == null) definition = findDefinitionInner(dictionary, search.toLowerCase());
			if(definition == null) definition = findDefinitionInner(dictionary, withoutX.toLowerCase());
		}
		return definition;
	}

	@Nullable
	private String findDefinitionInner(HashMap<String, String> dictionary, String search) {
		var definition = dictionary.get(search);
		if(definition == null && endsWithIgnoreCase(search, "n")) {
			var withoutN = search.substring(0, search.length() - 1);
			definition = dictionary.get(withoutN);

			if(definition == null && endsWithIgnoreCase(withoutN, "oj")) {
				var withoutJ = withoutN.substring(0, withoutN.length() - 1);
				definition = dictionary.get(withoutJ);
				if(definition == null) definition = findDefinitionNoun(dictionary, withoutJ);
			}
		}
		if(definition == null && endsWithIgnoreCase(search, "oj")) {
			var withoutJ = search.substring(0, search.length() - 1);
			definition = dictionary.get(withoutJ);
			if(definition == null) definition = findDefinitionNoun(dictionary, withoutJ);
		}
		if(definition == null && endsWithIgnoreCase(search, "o")) definition = findDefinitionNoun(dictionary, search);
		if(
			definition == null && (
				endsWithIgnoreCase(search, "as") ||
					endsWithIgnoreCase(search, "is") ||
					endsWithIgnoreCase(search, "os") ||
					endsWithIgnoreCase(search, "us")
			)
		) {
			definition = dictionary.get(search.substring(0, search.length() - 2) + "i");
		}
		if(definition == null && endsWithIgnoreCase(search, "u")) {
			definition = dictionary.get(search.substring(0, search.length() - 1) + "i");
		}
		if(definition == null && endsWithIgnoreCase(search, "e")) {
			// adverb, try adjective or noun forms
			definition = dictionary.get(search.substring(0, search.length() - 1) + "a");
			if(definition == null) {
				var asNoun = search.substring(0, search.length() - 1) + "o";
				definition = dictionary.get(asNoun);
				if(definition == null) definition = findDefinitionNoun(dictionary, asNoun);
			}
		}
		if(definition == null && endsWithIgnoreCase(search, "a")) {
			// adjective, try noun form
			var asNoun = search.substring(0, search.length() - 1) + "o";
			definition = dictionary.get(asNoun);
			if(definition == null) definition = findDefinitionNoun(dictionary, asNoun);
		}
		return definition;
	}

	@Nullable
	private String findDefinitionNoun(HashMap<String, String> dictionary, String search) {
		var prefix = search.substring(0, search.length() - 1);
		var definition = dictionary.get(prefix + "i"); // try verb form
		if(definition == null) definition = dictionary.get(prefix + "a"); // try adjective form

		return definition;
	}

	// See https://stackoverflow.com/a/38947571/2533397
	public static boolean endsWithIgnoreCase(String str, String suffix)
	{
		int suffixLength = suffix.length();
		return str.regionMatches(true, str.length() - suffixLength, suffix, 0, suffixLength);
	}
}
